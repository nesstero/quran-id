#!/usr/bin/env python
from setuptools import find_packages, setup

with open("README.md", "r") as des:
    l_desc = des.read()

setup(
    name='alquran_id',
    packages=find_packages(),
    version='0.0.3',
    include_package_data=True,
    package_data={'': ['source/*.xml']},
    description='Al-Quran, Terjemahan Indonesia dan Tafsir Jalalayn',
    url='https://gitlab.com/nesstero/quran-id',
    license='MIT',
    author='nestero',
    author_email='nestero@mail.com',
    long_description=l_desc,
    long_description_content_type='text/markdown',
)
