[![PyPI version](https://badge.fury.io/py/alquran-id.svg)](https://badge.fury.io/py/alquran-id)
# Al-Quran ID
Al Quran, Terjemahan indonesia, dan Tafsir Jalalayn

# Installasi
```shell
$ pip install alquran-id
```

# Menggunakan Al-Quran ID
```python
from alquran_id import AlQuran as Quran

def alquran(id_surat, id_ayat):
    quran = Quran()
    nama_surat = quran.Surat(id_surat)
    ar_ayt = quran.ArNumber(id_ayat)
    ayat = quran.Ayat(id_surat, id_ayat)
    jml_ayat = quran.JumlahAyat(id_surat)
    terjemahan = quran.Terjemahan(id_surat, id_ayat)
    tafsir = quran.Tafsir(id_surat, id_ayat)
    return nama_surat[0], nama_surat[1], ayat, jml_ayat, terjemahan, tafsir, ar_ayt

quran = alquran(1, 1)
print(f"""
Nama Surat      = {quran[0]} / {quran[1]}
Jumlah Ayat     = {quran[3]}
Ayat [{quran[6]}]
{quran[2]}
Terjemahan
{quran[4]}
Tafsir
{quran[5]}
""")
```
Output
```
Nama Surat      = الفاتحة / Al-Fatihah (Pembukaan)
Jumlah Ayat     = 7
Ayat [۱]
بسم ٱلله ٱلرحمـن ٱلرحيم
Terjemahan
Dengan menyebut nama Allah Yang Maha Pemurah lagi Maha Penyayang.
Tafsir
(Dengan nama Allah Yang Maha Pemurah lagi Maha Penyayang)
```

# Source
Source file xml diambil dari [Tanzil](http://tanzil.net)
